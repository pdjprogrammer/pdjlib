/* SPDX-License-Identifier: GPL-3.0 */

/* src/pdjlib.c
 *
 * Author : Padraig Johnston <pdjprogrammer@gmail.com>
 *
 * Some code based on ideas and code from:
 * CS50 Library for C <https://github.com/cs50/libcs50>
 *
 * Copyright (C) 2023 Padraig Johnston
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pdjlib.h"

/* Disable warnings from some compilers about the way we use variadic arguments */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"

/*
 * Number of strings allocated by get_string.
*/
static size_t allocations = 0;

/*
 * Array of strings allocated by get_string.
*/
static string *strings = NULL;

#undef get_string
string get_string(va_list *args, const char *format, ...)
{
    /* Check whether we have memory for another string */
    if (allocations == SIZE_MAX / sizeof(string)) {
        return NULL;
    }

    /* Growable buffer for characters */
    string buffer = NULL;

    /* Capacity of buffer */
    size_t capacity = 0;

    /* Number of characters actually in buffer */
    size_t size = 0;

    /* Character read or EOF */
    int c;

    /* Prompt user */
    if (format != NULL) {
        /* Initialize variadic argument list */
        va_list ap;

        /*
         * Code will pass in printf-like arguments as variadic parameters.
         * The programmer-facing get_string macro always sets args to
         * NULL. In this case, we initialize the list of variadic parameters
         * the standard way with va_start.
        */
        if (args == NULL) {
            va_start(ap, format);
        }

        /*
         * When functions in this library call get_string they will have
         * already stored their variadic parameters in a `va_list` and so they
         * just pass that in by pointer.
        */
        else {
            /* Put a copy of argument list in ap so it is not consumed by vprintf */
            va_copy(ap, *args);
        }

        /* Print prompt */
        vprintf(format, ap);

        /* Clean up argument list */
        va_end(ap);
    }

    /* Iteratively get characters from stdin, checking for CR (Mac OS), LF (Linux), and CRLF (Windows) */
    while ((c = fgetc(stdin)) != '\r' && c != '\n' && c != EOF) {
        /* Grow buffer if necessary */
        if (size + 1 > capacity) {
            /* Increment buffer's capacity if possible */
            if (capacity < SIZE_MAX) {
                capacity++;
            }
            else {
                free(buffer);
                return NULL;
            }

            /* Extend buffer's capacity */
            string temp = realloc(buffer, capacity);

            if (temp == NULL) {
                free(buffer);
                return NULL;
            }

            buffer = temp;
        }

        /* Append current character to buffer */
        buffer[size++] = c;
    }

    /* Check whether user provided no input */
    if (size == 0 && c == EOF) {
        return NULL;
    }

    /* Check whether user provided too much input (leaving no room for trailing NULL) */
    if (size == SIZE_MAX) {
        free(buffer);
        return NULL;
    }

    /* If last character read was CR, try to read LF as well */
    if (c == '\r' && (c = fgetc(stdin)) != '\n') {
        /* Return NULL if character can't be pushed back onto standard input */
        if (c != EOF && ungetc(c, stdin) == EOF) {
            free(buffer);
            return NULL;
        }
    }

    /* Minimize buffer */
    string s = realloc(buffer, size + 1);

    if (s == NULL) {
        free(buffer);
        return NULL;
    }

    /* Terminate string */
    s[size] = '\0';

    /* Resize array so as to append string */
    string *tmp = realloc(strings, sizeof(string) * (allocations + 1));

    if (tmp == NULL) {
        free(s);
        return NULL;
    }

    strings = tmp;

    /* Append string to array */
    strings[allocations] = s;
    allocations++;

    /* Return string */
    return s;
}

char get_char(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a char from user */
    while (true) {
        /* Get line of text, returning CHAR_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return CHAR_MAX;
        }

        /* Return a char if only a char was provided */
        char c, d;
        if (sscanf(line, "%c%c", &c, &d) == 1) {
            va_end(ap);
            return c;
        }
    }
}

double get_double(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a double from user */
    while (true) {
        /* Get line of text, returning DBL_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return DBL_MAX;
        }

        /* Return a double if only a double was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            double d = strtod(line, &tail);

            if (errno == 0 && *tail == '\0' && isfinite(d) != 0 && d < DBL_MAX) {
                /* Disallow hexadecimal and exponents */
                if (strcspn(line, "XxEePp") == strlen(line)) {
                    va_end(ap);
                    return d;
                }
            }
        }
    }
}

float get_float(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a float from user */
    while (true) {
        /* Get line of text, returning FLT_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return FLT_MAX;
        }

        /* Return a float if only a float was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            float f = strtof(line, &tail);

            if (errno == 0 && *tail == '\0' && isfinite(f) != 0 && f < FLT_MAX) {
                /* Disallow hexadecimal and exponents */
                if (strcspn(line, "XxEePp") == strlen(line)) {
                    va_end(ap);
                    return f;
                }
            }
        }
    }
}

int get_int(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get an int from user */
    while (true) {
        /* Get line of text, returning INT_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return INT_MAX;
        }

        /* Return an int if only an int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long n = strtol(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= INT_MIN && n < INT_MAX) {
                va_end(ap);
                return (int)n;
            }
        }
    }
}

long get_long(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a long from user */
    while (true) {
        /* Get line of text, returning LONG_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return LONG_MAX;
        }

        /* Return a long if only a long (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long n = strtol(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= LONG_MIN && n < LONG_MAX) {
                va_end(ap);
                return n;
            }
        }
    }
}

long long get_long_long(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a long long from user */
    while (true) {
        /* Get line of text, returning LLONG_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return LLONG_MAX;
        }

        /* Return a long long if only a long long (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long long n = strtoll(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= LLONG_MIN && n < LLONG_MAX) {
                va_end(ap);
                return n;
            }
        }
    }
}

/* 8-bit integers */
byte_t get_byte(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a signed 8-bit int from user */
    while (true) {
        /* Get line of text, returning BYTE_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return BYTE_MAX;
        }

        /* Return a signed 8-bit int if only a signed 8-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long n = strtol(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= BYTE_MIN && n < BYTE_MAX) {
                va_end(ap);
                return (byte_t)n;
            }
        }
    }
}

ubyte_t get_ubyte(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get an unsigned 8-bit int from user */
    while (true) {
        /* Get line of text, returning UBYTE_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return UBYTE_MAX;
        }

        /* Return an unsigned 8-bit int if only an unsigned 8-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            unsigned long n = strtoul(line, &tail, 10);

            /* Have to evaluate min value by seeing if 0 or greater than 0 to avoid compiler error */
            if (errno == 0 && *tail == '\0' && (n == 0 || n > 0) && n < UBYTE_MAX) {
                va_end(ap);
                return (ubyte_t)n;
            }
        }
    }
}

/* 16-bit integers */
word_t get_word(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a signed 16-bit int from user */
    while (true) {
        /* Get line of text, returning WORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return WORD_MAX;
        }

        /* Return a signed 16-bit int if only a signed 16-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long n = strtol(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= WORD_MIN && n < WORD_MAX) {
                va_end(ap);
                return (word_t)n;
            }
        }
    }
}

uword_t get_uword(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get an unsigned 16-bit int from user */
    while (true) {
        /* Get line of text, returning UWORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return UWORD_MAX;
        }

        /* Return an unsigned 16-bit int if only an unsigned 16-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            unsigned long n = strtoul(line, &tail, 10);

            /* Have to evaluate min value by seeing if 0 or greater than 0 to avoid compiler error */
            if (errno == 0 && *tail == '\0' && (n == 0 || n > 0) && n < UWORD_MAX) {
                va_end(ap);
                return (uword_t)n;
            }
        }
    }
}

/* 32-bit integers */
dword_t get_dword(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a signed 32-bit int from user */
    while (true) {
        /* Get line of text, returning DWORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return DWORD_MAX;
        }

        /* Return a signed 32-bit int if only a signed 32-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long n = strtol(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= DWORD_MIN && n < DWORD_MAX) {
                va_end(ap);
                return (dword_t)n;
            }
        }
    }
}

udword_t get_udword(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get an unsigned 32-bit int from user */
    while (true) {
        /* Get line of text, returning UDWORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return UDWORD_MAX;
        }

        /* Return an unsigned 32-bit int if only an unsigned 32-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            unsigned long n = strtoul(line, &tail, 10);

            /* Have to evaluate min value by seeing if 0 or greater than 0 to avoid compiler error */
            if (errno == 0 && *tail == '\0' && (n == 0 || n > 0) && n < UDWORD_MAX) {
                va_end(ap);
                return (udword_t)n;
            }
        }
    }
}

/* 64-bit integers */
qword_t get_qword(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get a signed 64-bit int from user */
    while (true) {
        /* Get line of text, returning QWORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return QWORD_MAX;
        }

        /* Return a signed 64-bit int if only a signed 64-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            long long n = strtoll(line, &tail, 10);

            if (errno == 0 && *tail == '\0' && n >= QWORD_MIN && n < QWORD_MAX) {
                va_end(ap);
                return (qword_t)n;
            }
        }
    }
}

uqword_t get_uqword(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    /* Try to get an unsigned 64-bit int from user */
    while (true) {
        /* Get line of text, returning UQWORD_MAX on failure */
        string line = get_string(&ap, format);

        if (line == NULL) {
            va_end(ap);
            return UQWORD_MAX;
        }

        /* Return an unsigned 64-bit int if only an unsigned 64-bit int (in range) was provided */
        if (strlen(line) > 0 && !isspace((unsigned char)line[0])) {
            char *tail;
            errno = 0;
            unsigned long long n = strtoull(line, &tail, 10);

            /* Have to evaluate min value by seeing if 0 or greater than 0 to avoid compiler error */
            if (errno == 0 && *tail == '\0' && (n == 0 || n > 0) && n < UQWORD_MAX) {
                va_end(ap);
                return (uqword_t)n;
            }
        }
    }
}

/*
 * Called automatically after execution exits main.
*/
static void teardown(void)
{
    /* Free library's strings */
    if (strings != NULL) {
        for (size_t i = 0; i < allocations; i++) {
            free(strings[i]);
        }

        free(strings);
    }
}

/*
 * Preprocessor magic to make initializers work somewhat portably
 * Modified from http://stackoverflow.com/questions/1113409/attribute-constructor-equivalent-in-vc
*/
#if defined (_MSC_VER) /* MSVC */
    #pragma section(".CRT$XCU",read)
    #define INITIALIZER_(FUNC,PREFIX) \
        static void FUNC(void); \
        __declspec(allocate(".CRT$XCU")) void (*FUNC##_)(void) = FUNC; \
        __pragma(comment(linker,"/include:" PREFIX #FUNC "_")) \
        static void FUNC(void)
    #ifdef _WIN64
        #define INITIALIZER(FUNC) INITIALIZER_(FUNC,"")
    #else
        #define INITIALIZER(FUNC) INITIALIZER_(FUNC,"_")
    #endif
#elif defined (__GNUC__) /* GCC, Clang, MinGW */
    #define INITIALIZER(FUNC) \
        static void FUNC(void) __attribute__((constructor)); \
        static void FUNC(void)
#else
    #error The PDJLIB library requires some compiler-specific features, \
           but we do not recognize this compiler/version. Please file an issue at \
           <github link>
#endif

/*
 * Called automatically before execution enters main.
*/
INITIALIZER(setup)
{
    /* Disable buffering for standard output */
    setvbuf(stdout, NULL, _IONBF, 0);
    atexit(teardown);
}

/* Re-enable warnings */
#pragma GCC diagnostic pop
