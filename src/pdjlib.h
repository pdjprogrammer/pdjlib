/* SPDX-License-Identifier: GPL-3.0 */

/* src/pdjlib.h
 *
 * Author : Padraig Johnston <pdjprogrammer@gmail.com>
 *
 * Some code based on ideas and code from:
 * CS50 Library for C <https://github.com/cs50/libcs50>
 *
 * Copyright (C) 2023 Padraig Johnston
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _PDJLIB_H
#define _PDJLIB_H

#include <float.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/*
 * Size specific data types for integers
*/
typedef int8_t   byte_t;       /* 8 bit signed */
typedef uint8_t  ubyte_t;      /* 8 bit unsigned */
typedef int16_t  word_t;       /* 16 bit signed */
typedef uint16_t uword_t;      /* 16 bit unsigned */
typedef int32_t  dword_t;      /* 32 bit signed */
typedef uint32_t udword_t;     /* 32 bit unsigned */
typedef int64_t  qword_t;      /* 64 bit signed */
typedef uint64_t uqword_t;     /* 64 bit unsigned */

/*
 * Defines of min/max of specific data types for integers
*/
#define BYTE_MIN INT8_MIN
#define BYTE_MAX INT8_MAX
#define UBYTE_MAX UINT8_MAX
#define WORD_MIN INT16_MIN
#define WORD_MAX INT16_MAX
#define UWORD_MAX UINT16_MAX
#define DWORD_MIN INT32_MIN
#define DWORD_MAX INT32_MAX
#define UDWORD_MAX UINT32_MAX
#define QWORD_MIN INT64_MIN
#define QWORD_MAX INT64_MAX
#define UQWORD_MAX UINT64_MAX

typedef char *string;

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent char.
 *
 * If text is not a single char, user is prompted to retry.
 * If line can't be read, returns CHAR_MAX.
*/
char get_char(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent double as precisely as possible.
 *
 * If text does not represent a double or if value would cause underflow or
 * overflow, user is prompted to retry.
 * If line can't be read, returns DBL_MAX.
*/
double get_double(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent float as precisely as possible.
 *
 * If text does not represent a float or if value would cause underflow or
 * overflow, user is prompted to retry.
 * If line can't be read, returns FLT_MAX.
*/
float get_float(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent int.
 *
 * If text does not represent an int in [-2^31, 2^31 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns INT_MAX.
*/
int get_int(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent long.
 *
 * If text does not represent a long in [-2^63, 2^63 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns LONG_MAX.
*/
long get_long(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent long long.
 *
 * If text does not represent a long long in [-2^63, 2^63 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns LLONG_MAX.
*/
long long get_long_long(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent signed 8-bit integer.
 *
 * If text does not represent a signed 8-bit int in [-2^7, 2^7 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns BYTE_MAX.
*/
byte_t get_byte(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent unsigned 8-bit integer.
 *
 * If text does not represent an unsigned 8-bit int in [0, 2^8 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns UBYTE_MAX.
*/
ubyte_t get_ubyte(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent signed 16-bit integer.
 *
 * If text does not represent a signed 16-bit int in [-2^15, 2^15 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns WORD_MAX.
*/
word_t get_word(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent unsigned 16-bit integer.
 *
 * If text does not represent an unsigned 16-bit int in [0, 2^16 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns UWORD_MAX.
*/
uword_t get_uword(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent signed 32-bit integer.
 *
 * If text does not represent a signed 32-bit int in [-2^31, 2^31 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns DWORD_MAX.
*/
dword_t get_dword(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent unsigned 32-bit integer.
 *
 * If text does not represent an unsigned 32-bit int in [0, 2^32 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns UDWORD_MAX.
*/
udword_t get_udword(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent signed 64-bit integer.
 *
 * If text does not represent a signed 64-bit int in [-2^63, 2^63 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns QWORD_MAX.
*/
qword_t get_qword(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns the
 * equivalent unsigned 64-bit integer.
 *
 * If text does not represent an unsigned 64-bit int in [0, 2^64 - 1)
 * or would cause underflow or overflow, user is prompted to retry.
 * If line can't be read, returns UQWORD_MAX.
*/
uqword_t get_uqword(const char *format, ...) __attribute__((format(printf, 1, 2)));

/*
 * Prompts user for a line of text from stdin and returns
 * it as a string (char *), sans trailing line ending.
 *
 * Supports CR (\r), LF (\n), and CRLF (\r\n) as line endings.
 * If user inputs only a line ending, returns "", not NULL.
 *
 * Returns NULL upon error or no input whatsoever (i.e., just EOF).
 * Stores string on heap, but library's destructor frees memory on program's exit.
*/
string get_string(va_list *args, const char *format, ...) __attribute__((format(printf, 2, 3)));
#define get_string(...) get_string(NULL, __VA_ARGS__)

#endif
