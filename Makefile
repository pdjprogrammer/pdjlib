VERSION := 1.0.0
MAJOR_VERSION := $(shell echo $(VERSION) | cut -d'.' -f1)

# installation directory (/usr/local by default)
DESTDIR ?= /usr/local
MANDIR ?= share/man/man3

SRC := src/pdjlib.c
INCLUDE := src/pdjlib.h
MANS := $(wildcard docs/*.3.gz)

CFLAGS=-Wall -Wextra -Werror -pedantic -std=c11
BASENAME=libpdjlib
LIB_STATIC=$(BASENAME).a
LIB_OBJ=$(BASENAME).o

OS := $(shell uname)

LIB_BASE := $(BASENAME).so
LIB_MAJOR := $(BASENAME).so.$(MAJOR_VERSION)
LIB_VERSION := $(BASENAME).so.$(VERSION)
LINKER_FLAGS := -Wl,-soname,$(LIB_MAJOR)

ifneq (,$(findstring MINGW64,$(MSYSTEM)))
	DESTDIR := /mingw64
else ifneq (,$(findstring MINGW32,$(MSYSTEM)))
	DESTDIR := /mingw32
else ifneq (,$(findstring UCRT64,$(MSYSTEM)))
	DESTDIR := /ucrt64
endif

LIBS := $(addprefix build/lib/, $(LIB_BASE) $(LIB_MAJOR) $(LIB_VERSION))

.PHONY: all
all: $(LIBS) $(MANS)

$(LIBS): $(SRC) $(INCLUDE) Makefile
	$(CC) $(CFLAGS) -fPIC -shared $(LINKER_FLAGS) -o $(LIB_VERSION) $(SRC)
	$(CC) $(CFLAGS) -c -o $(LIB_OBJ) $(SRC)
	ar rcs $(LIB_STATIC) $(LIB_OBJ)
	chmod 644 $(LIB_STATIC)
	rm -f $(LIB_OBJ)
	ln -sf $(LIB_VERSION) $(LIB_BASE)
	mkdir -p $(addprefix build/, include lib src)
	install -m 644 $(SRC) build/src
	install -m 644 $(INCLUDE) build/include
	mv $(LIB_VERSION) $(LIB_BASE) $(LIB_STATIC) build/lib

build/lib/$(LIB_BASE): build/lib/$(LIB_MAJOR)

build/lib/$(LIB_MAJOR): build/lib/$(LIB_VERSION)

.PHONY: install
install: all
	mkdir -p $(addprefix $(DESTDIR)/, src lib include $(MANDIR))
	cp -R $(filter-out deb, $(wildcard build/*)) $(DESTDIR)
	cp -R $(MANS) $(DESTDIR)/$(MANDIR)

ifeq ($(OS),Linux)
	ldconfig $(DESTDIR)/lib
endif

.PHONY: clean
clean:
	rm -rf build

.PHONY: version
version:
	@echo $(VERSION)

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)/include/pdjlib.h
	rm -rf $(DESTDIR)/src/pdjlib.c
	rm -f $(addprefix $(DESTDIR)/lib/, $(LIB_BASE) $(LIB_MAJOR) $(LIB_VERSION))
	rm -f $(addprefix $(DESTDIR)/$(MANDIR)/, get_*.3)
