# PDJLIB for C

## From Source (Linux)

1. Download the latest release from <https://gitlab.com/pdjprogrammer/pdjlib>
2. Extract `pdjlib-*.*`
3. `cd pdjlib-*`
4. `sudo make install`

By default, the library installs to `/usr/local`. If you'd like to change the installation location, run
`sudo DESTDIR=/path/to/install make install` as desired.

## From Source (Windows {msys2})

1. Download the latest release from <https://gitlab.com/pdjprogrammer/pdjlib>
2. Extract `pdjlib-*.*`
3. `cd pdjlib-*`
4. `make install`

By default, the library installs to `/mingw32, /mingw64, /ucrt64`, depending on which environment
you build the library in. If you'd like to change the installation location, run
`DESTDIR=/path/to/install make install` as desired.

### Troubleshooting

1. If, when compiling a program, you see `/usr/bin/ld: cannot find -lpdjlib`:
Add `export LIBRARY_PATH=/path/to/install/lib` to your `.bashrc`

2. If, when compiling a program, you see `fatal error: 'pdjlib.h' file not found`:
Add `export C_INCLUDE_PATH=/path/to/install/include` to your `.bashrc`.

3. If, when executing a program, you see `error while loading shared libraries: libpdjlib.so.8: cannot open shared object file: No such file or directory`:
Add `export LD_LIBRARY_PATH=/path/to/install/lib` to your `.bashrc`.

Close and reopen any terminal windows.

### Usage

Link with `-lpdjlib`.

    #include <pdjlib.h>

    ...
    char c = get_char("Prompt: ");
    double d = get_double("Prompt: ");
    float f = get_float("Prompt: ");
    int i = get_int("Prompt: ");
    long l = get_long("Prompt: ");
    string s = get_string("Prompt: ");
    byte_t = get_byte("Prompt: ");
    ubyte_t = get_ubyte("Prompt: ");
    word_t = get_word("Prompt: ");
    uword_t = get_uword("Prompt: ");
    dword_t = get_dword("Prompt: ");
    udword_t = get_udword("Prompt: ");
    qword_t = get_qword("Prompt: ");
    uqword_t = get_uqword("Prompt: ");

## Documentation

See `man get_*` after installation
